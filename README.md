<h1 align="center">
  Site do jogo Counter Strike 2
</h1>

<p align="center">
  <img src=".github/preview2.png" width="100%" />
</p>

## 💻 Project

This project is a study project where I create a website for the game **Counter Strike 2** using _HTML_ and _CSS_.

## 🚀 Technology

- HTML
- CSS
- [AOS Animate](https://michalsnik.github.io/aos/)

## 📔 Knowledge covered

- [x] Semantic use of HTML
- [x] Video loading in the background
- [x] Css variables in `:root`
- [x] Animations with the [AOS](https://michalsnik.github.io/aos/) library
- [x] Use of CSS Flexbox
- [x] Effects with css `transform` property

## 💻 Browse the site

https://cs2-gamesite.netlify.app

